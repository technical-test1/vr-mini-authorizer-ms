package br.com.vr.authorizer.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.util.Base64Utils.encodeToString;

import java.math.BigDecimal;
import java.util.Optional;

import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.vr.authorizer.WebApp;
import br.com.vr.authorizer.repository.CardAvailableLimitPersisteRepository;
import br.com.vr.authorizer.repository.CardPersisteRepository;
import br.com.vr.authorizer.repository.model.CardAvailableLimitModel;
import br.com.vr.authorizer.repository.model.CardModel;
import br.com.vr.authorizer.rest.request.TransactionRequest;
import br.com.vr.authorizer.util.TransactionEnum;

@AutoConfigureMockMvc
@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = { "classpath:application-test.yml" })
@SpringBootTest(classes = { WebApp.class} )
class TransactionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private CardPersisteRepository cardPersisteRepository;

	@MockBean
	private CardAvailableLimitPersisteRepository cardAvailableLimitPersisteRepository;

	@Test
	void test_transaction_with_available_balance() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
		.thenReturn( Optional.of(CardModel.builder().id(NumberUtils.LONG_ONE).numberCard("1234123412341234").password("123#123")
				.cardAvailableLimit(CardAvailableLimitModel.builder().id(NumberUtils.LONG_ONE).available(BigDecimal.TEN).build())
				.build() ) );

		BDDMockito.when( this.cardAvailableLimitPersisteRepository.findByCardId( ArgumentMatchers.anyLong()) )
		.thenReturn( CardAvailableLimitModel.builder().id(NumberUtils.LONG_ONE).available(BigDecimal.TEN).build() );

		var request = TransactionRequest.builder().numberCard("1234123412341234").password("123#123").value( BigDecimal.ONE ).build();

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.post("/transacoes").header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers
						.status()
						.isCreated())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		TransactionEnum response = objectMapper.readValue(contentAsString, new TypeReference<TransactionEnum>() {});

		assertEquals(  response , TransactionEnum.OK );

	}


	@Test
	void test_transaction_with_card_not_found_available_balance() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
		.thenReturn( Optional.empty() );

		BDDMockito.when( this.cardAvailableLimitPersisteRepository.findByCardId( ArgumentMatchers.anyLong()) )
		.thenReturn( CardAvailableLimitModel.builder().id(NumberUtils.LONG_ONE).available(BigDecimal.TEN).build() );

		var request = TransactionRequest.builder().numberCard("1234123412341234").password("123#123").value( BigDecimal.ONE ).build();

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.post("/transacoes").header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers
						.status()
						.isUnprocessableEntity())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		TransactionEnum response = objectMapper.readValue(contentAsString, new TypeReference<TransactionEnum>() {});

		assertEquals(  response , TransactionEnum.CARTAO_INEXISTENTE );

	}

	@Test
	void test_transaction_with_password_invalid_available_balance() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
		.thenReturn( Optional.of(CardModel.builder().id(NumberUtils.LONG_ONE).numberCard("1234123412341234").password("123#1231")
				.cardAvailableLimit(CardAvailableLimitModel.builder().id(NumberUtils.LONG_ONE).available(BigDecimal.TEN).build())
				.build() ) );

		var request = TransactionRequest.builder().numberCard("1234123412341234").password("123#123").value( BigDecimal.ONE ).build();

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.post("/transacoes").header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers
						.status()
						.isUnprocessableEntity())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		TransactionEnum response = objectMapper.readValue(contentAsString, new TypeReference<TransactionEnum>() {});

		assertEquals(  response , TransactionEnum.SENHA_INVALIDA );

	}

	@Test
	void test_transaction_with_insufficient_funds_available_balance() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
		.thenReturn( Optional.of(CardModel.builder().id(NumberUtils.LONG_ONE).numberCard("1234123412341234").password("123#123")
				.cardAvailableLimit(CardAvailableLimitModel.builder().id(NumberUtils.LONG_ONE).available(BigDecimal.ONE).build())
				.build() ) );

		var request = TransactionRequest.builder().numberCard("1234123412341234").password("123#123").value( BigDecimal.TEN ).build();

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.post("/transacoes").header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers
						.status()
						.isUnprocessableEntity())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		TransactionEnum response = objectMapper.readValue(contentAsString, new TypeReference<TransactionEnum>() {});

		assertEquals(  response , TransactionEnum.SALDO_INSUFICIENTE );
	}

}
