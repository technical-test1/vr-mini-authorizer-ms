package br.com.vr.authorizer.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.util.Base64Utils.encodeToString;

import java.math.BigDecimal;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.vr.authorizer.WebApp;
import br.com.vr.authorizer.repository.CardAvailableLimitPersisteRepository;
import br.com.vr.authorizer.repository.CardPersisteRepository;
import br.com.vr.authorizer.repository.model.CardAvailableLimitModel;
import br.com.vr.authorizer.repository.model.CardModel;
import br.com.vr.authorizer.rest.request.CardRequest;
import br.com.vr.authorizer.rest.response.CardResponse;

@AutoConfigureMockMvc
@ActiveProfiles(profiles = { "test" })
@TestPropertySource(locations = { "classpath:application-test.yml" })
@SpringBootTest(classes = { WebApp.class} )
class CardControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@MockBean
	private CardPersisteRepository cardPersisteRepository;

	@MockBean
	private CardAvailableLimitPersisteRepository cardAvailableLimitPersisteRepository;

	@Test
	void test_insert_card_with_success() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
        .thenReturn( Optional.empty() );

		BDDMockito.when( this.cardPersisteRepository.save( ArgumentMatchers.any( CardModel.class ) ) )
        .thenReturn( CardModel.builder().id(NumberUtils.LONG_ONE).numberCard("1234123412341234").password("123#123").build() );

		BDDMockito.when( this.cardAvailableLimitPersisteRepository.save( ArgumentMatchers.any( CardAvailableLimitModel.class ) ) )
        .thenReturn( CardAvailableLimitModel.builder().id(NumberUtils.LONG_ONE).available(BigDecimal.TEN).build() );

		var request = CardRequest.builder().numberCard("1234123412341234").password("123#123").build();

		MvcResult result = this.mockMvc.perform(
					MockMvcRequestBuilders
						.post("/cartoes")
						.header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		CardResponse response = objectMapper.readValue(contentAsString, new TypeReference<CardResponse>() {});

		assertNotNull(  response.getNumberCard().equals(request.getNumberCard() )  );
	}

	@Test
	void test_insert_card_with_numberCard_exist() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
        .thenReturn( Optional.of(CardModel.builder().id(NumberUtils.LONG_ONE).numberCard("1234123412341234").password("123#123").build() ) );

		var request = CardRequest.builder().numberCard("1234123412341234").password("123#123").build();

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.post("/cartoes").header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(request)))
				.andExpect(MockMvcResultMatchers.status().isUnprocessableEntity())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		CardResponse response = objectMapper.readValue(contentAsString, new TypeReference<CardResponse>() {});

		assertNotNull( response.getNumberCard().equals(request.getNumberCard() )  );
	}

	@Test
	void test_check_existing_card_balance() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
		.thenReturn( Optional.of(CardModel.builder().id(NumberUtils.LONG_ONE).numberCard("1234123412341234").password("123#123")
				.cardAvailableLimit(CardAvailableLimitModel.builder().id(NumberUtils.LONG_ONE).available(BigDecimal.TEN).build())
				.build() ) );

		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.get("/cartoes/1234123412341234").header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		BigDecimal response = objectMapper.readValue(contentAsString, new TypeReference<BigDecimal>() {});

		assertEquals( BigDecimal.TEN, response );
	}


	@Test
	void test_check_non_existing_card_balance() throws JsonProcessingException, Exception {

		BDDMockito.when( this.cardPersisteRepository.findByNumberCard( ArgumentMatchers.anyString() ) )
		.thenReturn( Optional.empty() );


		MvcResult result = this.mockMvc.perform(
				MockMvcRequestBuilders
				.get("/cartoes/1234123412341234").header(HttpHeaders.AUTHORIZATION, "Basic " + encodeToString("user@vr.com.br:user@123".getBytes()))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isNotFound())
				.andReturn();

		String contentAsString = result.getResponse().getContentAsString();

		assertEquals( contentAsString, StringUtils.EMPTY );
	}

}
