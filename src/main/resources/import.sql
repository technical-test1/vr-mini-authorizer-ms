-- { "name": "user", "email": "user@vr.com.br", "password": "user@123", "role": "ROLE_USER" }
-- { "name": "admin", "email": "admin@vr.com.br", "password": "admin@123", "role": "ROLE_USER" }

INSERT INTO user_profile (email,name,password,role) VALUES ('user@vr.com.br','user','$2a$10$km7OZTnK/ytcfR6J4/tiBusoyjjop3yA92JOgj79KRWEA8igctE5y','ROLE_USER');
INSERT INTO user_profile (email,name,password,role) VALUES ('admin@vr.com.br','admin','$2a$10$6CZ4sBVrK3SPvESvnrVN1eCzk2B.I.wzDpPhCYy22ReJT7Bjeb2ZK','ROLE_ADMIN');
commit;
