package br.com.vr.authorizer.util;

import java.util.List;

public interface ConvertMapper<Source,Target> {
	Target to ( Source source );
	List<Target> to ( List<Source> sources );
}