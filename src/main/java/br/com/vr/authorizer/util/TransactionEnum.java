package br.com.vr.authorizer.util;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
@Tag(name="TransactionEnum", description = "Retornos da transação")
@Schema(name = "TransactionRequest", description = "Retornos da transação")
public enum TransactionEnum {
	OK,
	SALDO_INSUFICIENTE,
	SENHA_INVALIDA,
	CARTAO_INEXISTENTE
}
