package br.com.vr.authorizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import br.com.vr.authorizer.infra.dto.ApiProperties;

@SpringBootApplication
@EnableConfigurationProperties( value = {ApiProperties.class} )
public class WebApp {

	public static void main(String[] args) {
		SpringApplication.run(WebApp.class, args);
	}

}
