package br.com.vr.authorizer.security.model;

import lombok.Data;

@Data
public class LoginCreds {

	private String email;
	private String password;
}
