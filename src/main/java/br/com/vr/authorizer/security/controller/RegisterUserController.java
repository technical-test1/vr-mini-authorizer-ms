package br.com.vr.authorizer.security.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.vr.authorizer.security.model.UserProfile;
import br.com.vr.authorizer.security.service.RegistrationService;
import lombok.RequiredArgsConstructor;

@RestController
@ConditionalOnProperty(value = "app.security.controller.enabled" , havingValue = "true")
@RequiredArgsConstructor
public class RegisterUserController {

	private final RegistrationService registerService;

	@PostMapping("/register")
	ResponseEntity<String> register(@RequestBody UserProfile user) {
		System.out.println("INSIDE");
		if(registerService.registerUser(user)) {
			return ResponseEntity.ok("User created successfully");
		}
		return ResponseEntity.badRequest().body("User created successfully");
	}
}
