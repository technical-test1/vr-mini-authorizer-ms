package br.com.vr.authorizer.security.controller;


import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.vr.authorizer.security.model.UserProfile;
import br.com.vr.authorizer.security.service.RegistrationService;
import lombok.RequiredArgsConstructor;

@RestController
@ConditionalOnProperty(value = "app.security.controller.enabled" , havingValue = "true")
@RequiredArgsConstructor
public class RegisterAdminController {

	private final RegistrationService registerService;

	@PostMapping("/register-admin")
	ResponseEntity<String> register(@RequestBody UserProfile admin) {
		if( registerService.registerAdmin(admin)) {
			return ResponseEntity.ok("Admin created successfully");
		}
		return ResponseEntity.badRequest().body("Admin already exists");
	}
}
