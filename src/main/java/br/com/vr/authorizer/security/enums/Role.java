package br.com.vr.authorizer.security.enums;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}