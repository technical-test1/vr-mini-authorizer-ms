package br.com.vr.authorizer.security.service;

import java.util.Optional;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.vr.authorizer.security.enums.Role;
import br.com.vr.authorizer.security.model.UserProfile;
import br.com.vr.authorizer.security.repository.RegisterNewUserRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RegistrationService {

	private final RegisterNewUserRepository registerRepo;

	private final PasswordEncoder pwdEncoder;

	public boolean registerUser(UserProfile user) {
		Optional<UserProfile> userContainer = registerRepo.findByEmail(user.getEmail());
		if(userContainer.isEmpty()) {
			user.setRole(Role.ROLE_USER);
			user.setPassword(pwdEncoder.encode(user.getPassword()));
			registerRepo.save(user);
			return true;
		}
		return false;
	}

	public boolean registerAdmin(UserProfile admin) {
		Optional<UserProfile> userContainer = registerRepo.findByEmail(admin.getEmail());
		if(userContainer.isEmpty()) {
			admin.setRole(Role.ROLE_ADMIN);
			admin.setPassword(pwdEncoder.encode(admin.getPassword()));
			registerRepo.save(admin);
			return true;
		}
		return false;
	}
}
