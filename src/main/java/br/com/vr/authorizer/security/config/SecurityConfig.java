package br.com.vr.authorizer.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import br.com.vr.authorizer.security.service.UserDetail;

@Configuration
@ConditionalOnProperty(value = "app.security.enabled" , havingValue = "true")
public class SecurityConfig {

	@Autowired
	private UserDetail userDetail;

	@Autowired
	private UnauthorizedEntrypoint unauthorizedEntrypoint;

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}


	@Bean
	SecurityFilterChain filterSecurity(HttpSecurity http) throws Exception {

		http.csrf(htpSecurity -> htpSecurity.disable());

		http.securityMatcher("/transacoes","/cartoes*","/contact", "/register*")
		.authorizeHttpRequests(
				(authorize) -> authorize
				.requestMatchers("/transacoes").hasAnyRole("USER","ADMIN")
				.requestMatchers("/cartoes*").hasAnyRole("USER","ADMIN")
				.requestMatchers("/contact").permitAll()
				.requestMatchers("/register*").permitAll()
				.requestMatchers("/swagger-ui*").permitAll()
				.anyRequest().authenticated())

				.httpBasic(httpSec -> httpSec.authenticationEntryPoint(unauthorizedEntrypoint)).userDetailsService(userDetail);
		return http.build();
	}

	@Bean
    WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().requestMatchers(new AntPathRequestMatcher("/swagger-ui/**"));
    }
}