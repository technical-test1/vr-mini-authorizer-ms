package br.com.vr.authorizer.security.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ConditionalOnProperty(value = "app.security.controller.enabled" , havingValue = "true")
public class HomeController {

	@GetMapping("/index")
	public String getIndex() {
		return "This is response to index api.";
	}

	@GetMapping("/admin-page")
	public String getAdmin() {
		return "This is response to admin api.";
	}

	@GetMapping("/contact")
	public String getContact() {
		return "This is response to contact api.";
	}

}
