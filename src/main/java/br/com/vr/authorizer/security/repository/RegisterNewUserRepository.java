package br.com.vr.authorizer.security.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.vr.authorizer.security.model.UserProfile;

@Repository
public interface RegisterNewUserRepository extends JpaRepository<UserProfile,Long>{

	Optional<UserProfile> findByEmail(String email);

}
