package br.com.vr.authorizer.rest.mapper;

import org.mapstruct.Mapper;

import br.com.vr.authorizer.domain.dto.CardDTO;
import br.com.vr.authorizer.rest.request.CardRequest;
import br.com.vr.authorizer.util.ConvertMapper;

@Mapper(componentModel = "spring", uses = {})
public interface CardRequestMapper extends ConvertMapper<CardRequest, CardDTO> {

}
