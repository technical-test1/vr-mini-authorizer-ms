package br.com.vr.authorizer.rest.api;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.vr.authorizer.rest.request.TransactionRequest;
import br.com.vr.authorizer.util.TransactionEnum;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "TransactionController", description = "Gerencia as transações do cartão")
public interface TransactionAPI {

	@Operation(summary = "Criar uma nova transação", description = "Criar uma nova transação",
			responses = {
					@ApiResponse(
							responseCode = "201",
							description = "Transação efetuada com sucesso",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = TransactionEnum.class))),
					@ApiResponse(
							responseCode = "422",
							description = "Não foi possivel efetuara transação",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = TransactionEnum.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Ocorreu um erro interno",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = String.class)))
	})
	ResponseEntity<TransactionEnum> transactionCard( TransactionRequest request );
}