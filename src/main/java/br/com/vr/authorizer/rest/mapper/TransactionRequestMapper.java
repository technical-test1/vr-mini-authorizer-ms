package br.com.vr.authorizer.rest.mapper;


import org.mapstruct.Mapper;
import br.com.vr.authorizer.domain.dto.TransactionDTO;
import br.com.vr.authorizer.rest.request.TransactionRequest;
import br.com.vr.authorizer.util.ConvertMapper;

@Mapper(componentModel = "spring", uses = {})
public interface TransactionRequestMapper extends ConvertMapper<TransactionRequest, TransactionDTO> {

}