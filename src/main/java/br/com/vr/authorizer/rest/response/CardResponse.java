package br.com.vr.authorizer.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class CardResponse {

	@JsonProperty("senha")
	private String password;

	@JsonProperty("numeroCartao")
	private String numberCard;
}
