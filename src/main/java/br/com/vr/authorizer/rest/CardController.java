package br.com.vr.authorizer.rest;

import java.math.BigDecimal;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vr.authorizer.domain.dto.CardDTO;
import br.com.vr.authorizer.domain.service.CardService;
import br.com.vr.authorizer.rest.api.CardAPI;
import br.com.vr.authorizer.rest.mapper.CardRequestMapper;
import br.com.vr.authorizer.rest.mapper.CardResponseMapper;
import br.com.vr.authorizer.rest.request.CardRequest;
import br.com.vr.authorizer.rest.response.CardResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/cartoes")
@RequiredArgsConstructor
@Slf4j
public class CardController implements CardAPI {

	private final CardService cardService;

	private final CardRequestMapper cardRequestMapper;
	private final CardResponseMapper cardResponseMapper;

	@Override
	@PostMapping
	public ResponseEntity<CardResponse> create(@RequestBody CardRequest cardRequest) {
		log.info("cardRequest");

		CardDTO cardDTO = cardRequestMapper.to(cardRequest);

		CardDTO cardDTOResponse = cardService.save(cardDTO);

		return ResponseEntity
				.status( HttpStatus.CREATED )
				.body(cardResponseMapper.to(cardDTOResponse));
	}

	@Override
	@GetMapping("/{numeroCartao}")
	public ResponseEntity<BigDecimal> findByCard(@PathVariable("numeroCartao") String numberCard) {
		log.info("findByCard" + numberCard);
		return ResponseEntity.ok(cardService.findAvailableLimit(numberCard));
	}

}
