package br.com.vr.authorizer.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@Tag(name="CardRequest", description = "Dados para gravar um novo cartão")
@Schema(name = "CardRequest", description = "Dados para gravar um novo cartão")
@AllArgsConstructor
@NoArgsConstructor
public class CardRequest {

	@JsonProperty("senha")
	@Schema(name = "senha", description = "Senha", example = "123#212" )
	private String password;

	@JsonProperty("numeroCartao")
	@Schema(name = "numeroCartao", description = "Número do novo cartão" , example = "1234123412341234")
	private String numberCard;

}
