package br.com.vr.authorizer.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.vr.authorizer.domain.dto.TransactionDTO;
import br.com.vr.authorizer.domain.service.TransactionService;
import br.com.vr.authorizer.rest.api.TransactionAPI;
import br.com.vr.authorizer.rest.request.TransactionRequest;
import br.com.vr.authorizer.util.TransactionEnum;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/transacoes")
@RequiredArgsConstructor
@Slf4j
public class TransactionController implements TransactionAPI {

	private final TransactionService transactionService;

	@Override
	@PostMapping
	public ResponseEntity<TransactionEnum> transactionCard( @RequestBody TransactionRequest transactionRequest ) {
		log.info("transactionRequest");

		transactionService.transactionCard(TransactionDTO.builder()
				.numberCard( transactionRequest.getNumberCard() )
				.password( transactionRequest.getPassword() )
				.value( transactionRequest.getValue() )
				.build());

		return ResponseEntity.status(HttpStatus.CREATED).body(TransactionEnum.OK);
	}
}
