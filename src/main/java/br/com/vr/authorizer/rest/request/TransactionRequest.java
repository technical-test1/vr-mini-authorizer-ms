package br.com.vr.authorizer.rest.request;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@Tag(name="TransactionRequest", description = "Informações necessárias para efetuar uma transação")
@Schema(name = "TransactionRequest", description = "Informações necessárias para efetuar uma transação")
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {

	@JsonProperty("senha")
	@Schema(name = "senha", description = "Senha", example = "123#212" )
	public String password;

	@JsonProperty("numeroCartao")
	@Schema(name = "numeroCartao", description = "Número do novo cartão" , example = "1234123412341234")
	public String numberCard;

	@JsonProperty("valor")
	@Schema(name = "valor", description = "Valor da transação" , example = "10.00")
	public BigDecimal value;

}