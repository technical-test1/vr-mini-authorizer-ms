package br.com.vr.authorizer.rest.api;

import java.math.BigDecimal;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import br.com.vr.authorizer.rest.request.CardRequest;
import br.com.vr.authorizer.rest.response.CardResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "CardController", description = "Gerencia a criação do cartãoe consulta de saldo")
public interface CardAPI {

	@Operation(summary = "Criar um novo cartão", description = "Criar um novo cartão",
			responses = {
					@ApiResponse(
							responseCode = "201",
							description = "Cartão criado com sucesso",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = CardResponse.class))),
					@ApiResponse(
							responseCode = "422",
							description = "Já existe um cartão com esse numero",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = CardResponse.class))),
					@ApiResponse(
							responseCode = "500",
							description = "Ocorreu um erro interno",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(implementation = String.class)))
	})
	ResponseEntity<CardResponse> create( CardRequest cad );

	@Operation(summary = "Consultar saldo do cartão", description = "Consulta o saldo do cartão",
			responses = {
					@ApiResponse(
							responseCode = "200",
							description = "Consulta o saldo com Sucesso",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ,
									schema = @Schema(implementation = BigDecimal.class))),
					@ApiResponse(
							responseCode = "404",
							description = "Saldo Inexistente",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE )),
					@ApiResponse(
							responseCode = "500",
							description = "Ocorreu um erro interno",
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE ))
	})
	ResponseEntity<BigDecimal> findByCard( String numberCard );
}