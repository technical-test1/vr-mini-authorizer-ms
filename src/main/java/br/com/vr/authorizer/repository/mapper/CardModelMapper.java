package br.com.vr.authorizer.repository.mapper;

import org.mapstruct.Mapper;

import br.com.vr.authorizer.domain.dto.CardDTO;
import br.com.vr.authorizer.repository.model.CardModel;
import br.com.vr.authorizer.util.ConvertMapper;

@Mapper(componentModel = "spring", uses = {})
public interface CardModelMapper extends ConvertMapper<CardDTO, CardModel>{

}
