package br.com.vr.authorizer.repository.model;

import java.io.Serializable;
import java.math.BigDecimal;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Table
@Entity( name = "TBPVR_CARD_AVAILABLE")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
public class CardAvailableLimitModel implements Serializable {

		@Id
		@Column(name = "CARD_ID")
		//@SequenceGenerator(name = "TBPVR_CARD_AVAILABLE_GENERATOR", sequenceName = "SEQ_CARD_AVAILABLE", initialValue = 1, allocationSize = 1)
		@GeneratedValue(strategy = GenerationType.IDENTITY/*, generator = "TBPVR_CARD_AVAILABLE_GENERATOR"*/ )
		@EqualsAndHashCode.Include
		private Long id;

	    @OneToOne
	    @JoinColumn( name = "CARD_ID", referencedColumnName = "CARD_ID" , updatable = false, insertable = true )
	    private CardModel card;

		@Column(name = "AVAILABLE")
		public BigDecimal available;

}
