package br.com.vr.authorizer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.vr.authorizer.repository.model.CardAvailableLimitModel;

@Repository
public interface CardAvailableLimitPersisteRepository extends JpaRepository<CardAvailableLimitModel, Long> {

	CardAvailableLimitModel findByCardId( Long cardId );

}
