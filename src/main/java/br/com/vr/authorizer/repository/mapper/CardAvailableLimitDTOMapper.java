package br.com.vr.authorizer.repository.mapper;

import org.mapstruct.Mapper;

import br.com.vr.authorizer.domain.dto.CardAvailableLimitDTO;
import br.com.vr.authorizer.repository.model.CardAvailableLimitModel;
import br.com.vr.authorizer.util.ConvertMapper;

@Mapper(componentModel = "spring", uses = {})
public interface CardAvailableLimitDTOMapper extends ConvertMapper<CardAvailableLimitModel, CardAvailableLimitDTO> {

}
