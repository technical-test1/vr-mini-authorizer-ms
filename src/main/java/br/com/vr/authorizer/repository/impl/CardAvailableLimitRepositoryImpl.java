package br.com.vr.authorizer.repository.impl;

import java.math.BigDecimal;

import org.springframework.stereotype.Repository;

import br.com.vr.authorizer.domain.repository.CardAvailableLimitRepository;
import br.com.vr.authorizer.repository.CardAvailableLimitPersisteRepository;
import br.com.vr.authorizer.repository.model.CardAvailableLimitModel;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Repository
@RequiredArgsConstructor
public class CardAvailableLimitRepositoryImpl implements CardAvailableLimitRepository {

	private final CardAvailableLimitPersisteRepository availableLimitPersisteRepository;

	@Override
	@Transactional
	public void updateAvailableLimit(Long idCard, BigDecimal availableLimit) {

		CardAvailableLimitModel model = availableLimitPersisteRepository.findByCardId(idCard);
		model.setAvailable(availableLimit);
		availableLimitPersisteRepository.save(model);
		availableLimitPersisteRepository.flush();

	}

}
