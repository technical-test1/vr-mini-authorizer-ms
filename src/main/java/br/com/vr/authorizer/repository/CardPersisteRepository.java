package br.com.vr.authorizer.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.vr.authorizer.repository.model.CardModel;


@Repository
public interface CardPersisteRepository extends JpaRepository<CardModel, Long>{

	Optional<CardModel> findByNumberCard(String numberCard);
}
