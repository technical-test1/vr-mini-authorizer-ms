package br.com.vr.authorizer.repository.model;

import java.io.Serializable;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Table
@Entity( name = "TBPVR_CARD")
@Data
@Builder
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@AllArgsConstructor
@NoArgsConstructor
public class CardModel implements Serializable {

		@Id
		@SequenceGenerator(name = "TBPVR_CARD_GENERATOR", sequenceName = "SEQ_CARD", initialValue = 1, allocationSize = 1)
		@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TBPVR_CARD_GENERATOR")
		@Column(name = "CARD_ID")
		@EqualsAndHashCode.Include
		private Long id;

		@Column(name = "ACCESS_PASS")
		public String password;

		@Column(name = "NUMBER_CARD")
		public String numberCard;

	    @OneToOne(cascade=CascadeType.PERSIST)
		@JoinColumn(name = "CARD_ID"  )
		@ToString.Exclude
		public CardAvailableLimitModel cardAvailableLimit;

	    public void setCardAvailableLimit( CardAvailableLimitModel cardAvailableLimit ) {
	    	if(cardAvailableLimit !=null ) {
	    		cardAvailableLimit.setCard(this);
	    	}
	    	this.cardAvailableLimit = cardAvailableLimit;
	    }
}
