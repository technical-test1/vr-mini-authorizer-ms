package br.com.vr.authorizer.repository.impl;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vr.authorizer.domain.dto.CardDTO;
import br.com.vr.authorizer.domain.repository.CardRepository;
import br.com.vr.authorizer.repository.CardPersisteRepository;
import br.com.vr.authorizer.repository.mapper.CardDTOMapper;
import br.com.vr.authorizer.repository.mapper.CardModelMapper;
import br.com.vr.authorizer.repository.model.CardAvailableLimitModel;
import br.com.vr.authorizer.repository.model.CardModel;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CardRespositoryImpl implements CardRepository {

	private final CardPersisteRepository cardPersisteRepository;

	private final CardModelMapper cardModelMapper;

	private final CardDTOMapper cardDTOMapper;

	@Value("${app.balance-initial:999}")
	private BigDecimal limit = BigDecimal.ZERO;

	@Override
	@Transactional
	public CardDTO save(CardDTO cardDTO) {
		CardModel model = cardModelMapper.to(cardDTO);
		CardAvailableLimitModel cardAvailableLimit = CardAvailableLimitModel.builder().available(limit).build();
		model.setCardAvailableLimit(cardAvailableLimit);
		CardModel response = cardPersisteRepository.save(model);
		return cardDTOMapper.to(response);
	}

	@Override
	public Boolean isExist(String numberCard) {
		Optional<CardModel> response = cardPersisteRepository.findByNumberCard(numberCard);
		return response.isPresent();
	}

	@Override
	public BigDecimal getAvailableLimit(String numberCard) {
		Optional<CardModel> response = cardPersisteRepository.findByNumberCard(numberCard);
		return response.get().getCardAvailableLimit().getAvailable();
	}

	@Override
	public CardDTO findByNumberCard( String numberCard ) {

		Optional<CardModel> response = cardPersisteRepository.findByNumberCard(numberCard);

		if(response.isEmpty()) {
			return null;
		}

		return cardDTOMapper.to(response.get());
	}

}
