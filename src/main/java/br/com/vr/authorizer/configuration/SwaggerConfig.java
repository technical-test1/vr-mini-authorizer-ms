package br.com.vr.authorizer.configuration;

import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;

@Configuration
@OpenAPIDefinition(info = @Info(
    title = "Leandro Marques da Cunha - Teste tecnico - VR",
    description = "Mini Autorizador VR",
    version = "${app.version}",
    contact = @Contact(email="leandromcunha@gmail.com",
    					name="Leandro Marques da Cunha"),
    summary = "Mini Autorizador de transações"
))
public class SwaggerConfig {}