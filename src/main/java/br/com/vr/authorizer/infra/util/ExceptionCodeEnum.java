package br.com.vr.authorizer.infra.util;

import org.springframework.http.HttpStatus;

public enum ExceptionCodeEnum {

    INTERNAL_SERVER_ERROR( HttpStatus.INTERNAL_SERVER_ERROR),
    CARD_WITH_THE_SAME_NUMBER_ALREADY_EXISTS(HttpStatus.UNPROCESSABLE_ENTITY),
    CARD_NOT_FOUND(HttpStatus.NOT_FOUND),
    INSUFFICIENT_FUNDS(HttpStatus.UNPROCESSABLE_ENTITY),
    CARTAO_INEXISTENTE(HttpStatus.UNPROCESSABLE_ENTITY),
    INVALID_PASSWORD(HttpStatus.UNPROCESSABLE_ENTITY),
    CARD_INEXISTENT(HttpStatus.UNPROCESSABLE_ENTITY),
    ;

    private HttpStatus httpStatus;

    ExceptionCodeEnum( final HttpStatus httpStatus ) {
        this.httpStatus = httpStatus;
    }

    public String getCode() {
        return this.name();
    }

    public HttpStatus getHttpStatus() {
        return this.httpStatus;
    }
}
