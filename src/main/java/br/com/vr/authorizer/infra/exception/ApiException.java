package br.com.vr.authorizer.infra.exception;


import org.apache.commons.lang3.StringUtils;

import br.com.vr.authorizer.infra.util.ExceptionCodeEnum;

public class ApiException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 5127042490049753890L;
	private final ExceptionCodeEnum exceptionCodeEnum;
	private final Object bodyResponse;


	protected ApiException(final ExceptionCodeEnum exceptionCodeEnum, Object bodyResponse , final Throwable e ) {
		super(e);
		this.exceptionCodeEnum = exceptionCodeEnum;
		this.bodyResponse = bodyResponse;
	}

	public ApiException(final ExceptionCodeEnum exceptionCodeEnum, Object bodyResponse  ) {
		this(exceptionCodeEnum,bodyResponse,null);
	}

	public ApiException(final ExceptionCodeEnum exceptionCodeEnum, final Throwable e ) {
		this(exceptionCodeEnum,null,e);
	}

	public ApiException(final ExceptionCodeEnum exceptionCodeEnum) {
		this(exceptionCodeEnum,StringUtils.EMPTY, null);
	}

	public Object getBodyResponse() {
		return bodyResponse;
	}
	/**
	 * @return {@link ExceptionCodeEnum}
	 */
	public ExceptionCodeEnum getExceptionCodeEnum() {
		return this.exceptionCodeEnum;
	}
}
