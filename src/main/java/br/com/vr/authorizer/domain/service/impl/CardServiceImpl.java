package br.com.vr.authorizer.domain.service.impl;

import java.math.BigDecimal;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;

import br.com.vr.authorizer.domain.dto.CardDTO;
import br.com.vr.authorizer.domain.repository.CardRepository;
import br.com.vr.authorizer.domain.service.CardService;
import br.com.vr.authorizer.infra.exception.ApiException;
import br.com.vr.authorizer.infra.util.ExceptionCodeEnum;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

	private final CardRepository cardRepository;

	@Override
	public CardDTO save(CardDTO cardDTO) {

		if( cardRepository.isExist(cardDTO.getNumberCard())) {
			throw new ApiException(ExceptionCodeEnum.CARD_WITH_THE_SAME_NUMBER_ALREADY_EXISTS, cardDTO);
		}

		return cardRepository.save(cardDTO);
	}

	@Override
	public BigDecimal findAvailableLimit(String numberCard) {

		if( BooleanUtils.isFalse( cardRepository.isExist( numberCard ) ) ) {
			throw new ApiException( ExceptionCodeEnum.CARD_NOT_FOUND );
		}

		return cardRepository.getAvailableLimit(numberCard);
	}

}

