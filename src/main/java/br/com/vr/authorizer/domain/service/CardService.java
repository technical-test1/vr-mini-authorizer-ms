package br.com.vr.authorizer.domain.service;

import java.math.BigDecimal;

import br.com.vr.authorizer.domain.dto.CardDTO;

public interface CardService {

	public CardDTO save(CardDTO cardDTO );

	public BigDecimal findAvailableLimit(String numberCard );

}