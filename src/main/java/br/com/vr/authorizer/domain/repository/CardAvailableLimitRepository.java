package br.com.vr.authorizer.domain.repository;

import java.math.BigDecimal;

public interface CardAvailableLimitRepository {

	public void updateAvailableLimit( Long idCard , BigDecimal availableLimit );

}
