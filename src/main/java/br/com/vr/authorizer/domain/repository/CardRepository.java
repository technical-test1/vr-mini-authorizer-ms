package br.com.vr.authorizer.domain.repository;

import java.math.BigDecimal;

import br.com.vr.authorizer.domain.dto.CardDTO;

public interface CardRepository {

	CardDTO save( CardDTO cardDTO );

	Boolean isExist( String numberCard );

	BigDecimal getAvailableLimit(String numberCard );

	CardDTO findByNumberCard( String numberCard );

}
