package br.com.vr.authorizer.domain.service;

import br.com.vr.authorizer.domain.dto.TransactionDTO;

public interface TransactionService {

	public void transactionCard( TransactionDTO transactionDTO );

}
