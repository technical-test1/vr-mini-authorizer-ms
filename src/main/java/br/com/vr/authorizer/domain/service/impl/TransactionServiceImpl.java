package br.com.vr.authorizer.domain.service.impl;

import java.math.BigDecimal;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.vr.authorizer.domain.dto.CardDTO;
import br.com.vr.authorizer.domain.dto.TransactionDTO;
import br.com.vr.authorizer.domain.repository.CardAvailableLimitRepository;
import br.com.vr.authorizer.domain.repository.CardRepository;
import br.com.vr.authorizer.domain.service.TransactionService;
import br.com.vr.authorizer.infra.exception.ApiException;
import br.com.vr.authorizer.infra.util.ExceptionCodeEnum;
import br.com.vr.authorizer.util.TransactionEnum;
import jakarta.persistence.LockModeType;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

	private final CardAvailableLimitRepository cardAvailableLimitRepository;

	private final CardRepository cardRepository;

	@Override
	@Transactional
	@Lock(LockModeType.PESSIMISTIC_READ)
	public void transactionCard(TransactionDTO transactionDTO) {

		CardDTO cardDTo = cardRepository.findByNumberCard( transactionDTO.getNumberCard());

		if( cardDTo == null ) {
			throw new ApiException(ExceptionCodeEnum.CARD_INEXISTENT,TransactionEnum.CARTAO_INEXISTENTE);
		}

		if(  BooleanUtils.isFalse( cardDTo.getPassword().equals( transactionDTO.getPassword() )) ) {
			throw new ApiException(ExceptionCodeEnum.INVALID_PASSWORD,TransactionEnum.SENHA_INVALIDA);
		}

		BigDecimal limit = cardDTo.getCardAvailableLimit().getAvailable().subtract( transactionDTO.getValue() );

		if( limit.compareTo( BigDecimal.ZERO ) == -1) {
			throw new ApiException(ExceptionCodeEnum.INSUFFICIENT_FUNDS,TransactionEnum.SALDO_INSUFICIENTE);
		}

		cardAvailableLimitRepository.updateAvailableLimit(cardDTo.getId(), limit );
	}

}