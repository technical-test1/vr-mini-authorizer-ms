package br.com.vr.authorizer.domain.dto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardDTO {

	@JsonIgnore
	private Long id;

	@JsonProperty("senha")
	private String password;

	@JsonProperty("numeroCartao")
	private String numberCard;

	@JsonIgnore
	private CardAvailableLimitDTO cardAvailableLimit;

}
